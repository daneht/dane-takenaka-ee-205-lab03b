///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.h
/// @version 1.0
///
/// Exports data about cats
///
/// @author Dane Takenaka <daneht@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   2/2/2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "animals.h"

/// @todo declare a struct Cat here

struct Cat {

   enum Gender {MALE, FEMALE} gender;
   enum Color  {BLACK, WHITE, RED, BLUE, GREEN, PINK} collar1_color, collar2_color;
   enum Breed  {MAIN_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX} breed;
   char           name[32];
   bool           isFixed;
   float          weight;
   long           license;
};




/// Add Alice to the Cat catabase at position i.
/// 
/// @param int i Where in the catDB array that Alice the cat will go
///
/// @note This terrible style... we'd never hardcode this data, but it gets us started.
void addAliceTheCat(int i) ;



/// Print a cat from the database
/// 
/// @param int i Which cat in the database that should be printed
///
void printCat(int i) ;

