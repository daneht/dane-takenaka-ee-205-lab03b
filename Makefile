###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author Dane Takenaka <daneht@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   2/2/2021
###############################################################################


animalfarm: main.o cat.o animals.o
	gcc -o animalfarm *.c

clean:
	rm *.o animalfarm


